<?php

/**
 * @file
 * Theme functions for Analytics functions for share progress API.
 */

use \Drupal\share_progress\Drupal\Theme;

/**
 * Network theme (not currently used).
 */
function theme_share_progress_analytics_network($variables) {
  return Theme::instantiate()->analyticsNetwork($variables);
}

/**
 * Tool theme.
 */
function theme_share_progress_analytics_tool($variables) {
  return Theme::instantiate()->analyticsTool($variables);
}

/**
 * Campaign theme.
 */
function theme_share_progress_analytics_campaign($variables) {
  return Theme::instantiate()->analyticsCampaign($variables);
}

/**
 * Org theme.
 */
function theme_share_progress_analytics_org($variables) {
  return Theme::instantiate()->analyticsOrg($variables);
}

/**
 * Image preview for uploaded images.
 */
function theme_share_progress_image_preview($variables) {
  return Theme::imagePreview($variables);
}
