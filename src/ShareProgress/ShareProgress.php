<?php

namespace Drupal\share_progress\ShareProgress;

/**
 * Share Progress API Class.
 */
class ShareProgress {
  const URL = 'https://run.shareprogress.org/api/';
  const VERSION = 'v1';

  private $key = '';
  private $type = '';

  /**
   * Constructor.
   */
  public function __construct($key, $type = 'button', $url = self::URL) {
    if (!$key) {
      throw new \Exception('You must specify a valid key');
    }
    $this->key = $key;
    $this->type = $type;
  }

  /**
   * Instantiator.
   */
  static public function instantiate($type = 'buttons') {
    static $ob = array();
    if (!isset($ob[$type])) {
      $key = variable_get('share_progress_key', '');
      $ob[$type] = new ShareProgress($key, $type);
    }
    return $ob[$type];
  }

  /**
   * Method to create or update a button.
   */
  public function update($data) {
    return $this->api('update', $data);
  }

  /**
   * Method to delete a button.
   */
  public function delete($id) {
    return $this->api('delete', $id);
  }

  /**
   * Method to get analytics.
   */
  public function analyze($id) {
    $cid = 'sp:' . $id;
    if ($cid && $cache = cache_get($cid)) {
      return $cache->data;
    }
    else {
      $response = $this->api('analytics', $id);
      if (is_array($response)) {
        cache_set($cid, $response, 'cache', strtotime('+1 hour'));
        return $response;
      }
    }
  }

  /**
   * Method to just get totals from analytics.
   */
  public function analyzeTotals($id) {
    $analytics = $this->analyze($id);
    if (!empty($analytics)) {
      return $analytics['total'];
    }
  }

  /**
   * Method to get A/B stats from analytics.
   */
  public function analyzeAb($id) {
    $analytics = $this->analyze($id);
    if (!empty($analytics)) {
      return $analytics['share_tests'];
    }
  }

  /**
   * Method to return a winner in A/B testing.
   */
  public function analyzeAbWinner($id, $network) {
    $ab = $this->analyzeAb($id);
    if (isset($ab[$network])) {
      foreach ($ab[$network] as $key => $test) {
        if ($test['winner']) {
          return $key;
        }
      }
    }
    return FALSE;
  }

  /**
   * Makes calls to Share Progress API.
   */
  private function api($method, $data) {
    if (!is_array($data)) {
      $data = array('id' => $data);
    }

    $data['key'] = $this->key;

    $url = self::URL . self::VERSION . '/' . $this->type . '/' . $method;

    $parameters = array(
      'method' => 'POST',
      'headers' => array('Content-Type' => 'application/json'),
      'data' => json_encode($data),
    );

    try {
      $response = drupal_http_request($url, $parameters);
      $decoded = json_decode($response->data, TRUE);
      if (!$decoded['success']) {
        throw new \Exception($decoded['message']);
      }
      $decoded = $decoded['response'][0];
      return $decoded;
    }
    catch (\Exception $e) {
      watchdog('share_progress', 'Share Progress API failure: %e',
        array('%e' => $e->getMessage()), WATCHDOG_ERROR);
      return FALSE;
    }
  }

}
