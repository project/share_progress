<?php

namespace Drupal\share_progress\traits;

/**
 * Trait for interacting with Drupal through mockable methods.
 */
trait Environment {

  /**
   * Mockable wrapper around check_plain().
   */
  protected function checkPlain($string) {
    return check_plain($string);
  }

  /**
   * Mockable wrapper around drupal_alter().
   */
  protected function drupalAlter($type, &$data) {
    return drupal_alter($type, $data);
  }

  /**
   * Mockable wrapper around cache_set().
   */
  protected function cacheSet($cid, $data, $expire = 0, $headers = NULL) {
    return cache_set($cid, $data, $expire, $headers);
  }

  /**
   * Mockable wrapper around cache_get().
   */
  protected function cacheGet($cid, $bin = 'cache') {
    return cache_get($cid, $bin);
  }

  /**
   * Mockable wrapper around drupal_http_request().
   */
  protected function drupalHTTPRequest($url, $parameters) {
    return drupalHTTPRequest($url, $parameters);
  }

  /**
   * Mockable wrapper around t().
   */
  protected function t(string $string) {
    // @codingStandardsIgnoreStart
    return t($string);
    // @codingStandardsIgnoreEnd
  }

  /**
   * Mockable wrapper around libraries_get_path().
   */
  protected function librariesGetPath(string $library) {
    return libraries_get_path($library);
  }

  /**
   * Mockable wrapper around drupal_get_path().
   */
  protected function drupalGetPath(string $type, string $asset) {
    return drupal_get_path($type, $asset);
  }

}
