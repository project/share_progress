<?php

namespace Drupal\share_progress\Drupal;

use \Drupal\share_progress\ShareProgress\ShareProgress;

/**
 * Class for local share_progress variant data.
 */
class Variants {
  private $node;
  private $network;
  private $spid;
  public $variants = array();

  /**
   * Constructor.
   *
   * @param $node
   *   share progress Node object
   * @param $network
   *   twitter or facebook or email
   */
  function __construct(Node $node) {
    $this->node = $node;
    $this->spid = $node->get('spid');
    $this->network = $node->network;
    if ($data = $node->data()) {
      $this->variants = unserialize($data['variants'])[$this->network];
    }
  }

  /**
   * Return a count of variants, defaulting to 1.
   */
  public function count() {
    return count($this->variants);
  }

  /**
   * Generate the form.
   */
  public function form($input) {
    $variants = $this->variants;
    $vset = array (
      '#type' => 'fieldset',
    );

    $winner = -1;
    if ($this->count() > 1 && $this->node->get('enabled')) {
      $vset[$this->network . '_ab_warning'] = array(
        '#markup' => '<div class="sp_ab_warning"><strong>Warning: You are currently A/B testing. Any changes made here could invalidate the results of that testing.</strong></div>',
      );
      $winner = ShareProgress::instantiate()->analyzeAbWinner($this->spid, $this->network);
    }

    // Render all variants but only show the number requested.
    for ($i=0; $i < 3; $i++) {

      $visible = array(
        'visible' => array(
          array(
            array(
              ':input[name="' . $this->network . '_count"]' => ['value' => $i+1]
            ),
            'or',
            array(
              ':input[name="' . $this->network . '_count"]' => ['value' => $i+2]
            ),
            'or',
            array(
              ':input[name="' . $this->network . '_count"]' => ['value' => $i+3]
            ),
          ),
        ),
      );

      $vid = $this->network . '_variant_id_' . $i;
      $vset[$vid] = array(
        '#type' => 'hidden',
        '#default_value' => isset($input[$vid]) ? $input[$vid] :
          (isset($variants[$i]['id']) ? $variants[$i]['id'] : ''),
      );

      if ($i === $winner) {
        $vset[$this->network . '_winner'] = array(
          '#markup' => '<div id="variant_winner"><strong>AB Testing: </strong>This button is currently performing the best in A/B testing.</div>',
        );
      }

      $method = $this->network . 'Form';
      if (method_exists(__CLASS__, $method)) {
        $vset += $this->$method($i, $input, $visible);
      }

    }
    return $vset;
  }

  private function twitterForm($i, $input, $visible) {
    $variant = isset($this->variants[$i]) ? $this->variants[$i] : array('twitter_message' => '{LINK}');
    $text_id = 'twitter_share_text_' . $i;
    return array(
      $text_id => array(
        '#type' => 'textarea',
        '#title' => $i > 0 ? t('Share Text: version ') . ($i + 1) : t('Share Text'),
        '#description' => t('The content you would like your supporters to share. <strong>The {LINK} placeholder must be placed somewhere within the text</strong> and will be replaced by the Campaign URL.'),
        '#maxlength' => 258,
        '#default_value' => isset($input[$text_id]) ? $input[$text_id] : $variant['twitter_message'],
        '#attributes' => array('class' => array('sp-twitter-text')),
        '#states' => $visible,
      )
    );
  }

  private function facebookForm($i, $input, $visible) {
    $variant = isset($this->variants[$i]) ? $this->variants[$i] : array(
      'facebook_title' => '',
      'facebook_description' => '',
      'facebook_thumbnail' => '',
    );
    $title_id = 'facebook_title_' . $i;
    $desc_id = 'facebook_description_' . $i;
    $thumb_id = 'facebook_thumbnail_' . $i;
    $ret = array(
      $title_id => array(
        '#type' => 'textfield',
        '#title' => $i > 0 ? t('Title: version ') . ($i + 1) : t('Title'),
        '#description' => t('When supporters share on Facebook, what should the title of the post be?'),
        '#default_value' => isset($input[$title_id]) ? $input[$title_id] : $variant['facebook_title'],
        '#maxlength' => 100,
        '#states' => $visible,
      ),
      $desc_id => array(
        '#type' => 'textarea',
        '#title' => $i > 0 ? t('Description: version ') . ($i + 1) : t('Description'),
        '#description' => t('When supporters share on Facebook, what should the description below the title be?'),
        '#default_value' => isset($input[$desc_id]) ? $input[$desc_id] : $variant['facebook_description'],
        '#maxlength' => 260,
        '#attributes' => array('class' => array('sp-facebook-text')),
        '#states' => $visible,
      ),
      $thumb_id => array(
        '#type' => 'managed_file',
        '#upload_location' => 'public://share_progress/' . $this->node->nid . '/',
        '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
        '#title' => $i > 0 ? t('Thumbnail Image: version ') . ($i + 1) : t('Thumbnail_image'),
        '#description' => t('Upload a 1.91:1 aspect ratio image (gif, png or jpg) to appear with shared Facebook posts. Optimal image size is 1200x630.'),
        '#theme' => 'share_progress_image_preview',
        '#default_value' => isset($input[$thumb_id]) ? $input[$thumb_id] : $variant['facebook_thumbnail'],
        '#states' => $visible,
      )
    );
    return $ret;
  }

  private function emailForm($i, $input, $visible) {
    $subject_id = 'email_subject_' . $i;
    $body_id = 'email_body_' . $i;
    $variant = isset($this->variants[$i]) ? $this->variants[$i] : array(
      'email_subject' => '',
      'email_body' => '{LINK}',
    );
    $ret = array(
      $subject_id => array(
        '#type' => 'textfield',
        '#title' => $i > 0 ? t('Subject: version ') . ($i + 1) : t('Subject'),
        '#description' => t('When supporters share by email, what should the email subject be?'),
        '#default_value' => isset($input[$subject_id]) ? $input[$subject_id] : $variant['email_subject'],
        '#maxlength' => 100,
        '#states' => $visible,
      ),
      $body_id => array(
        '#type' => 'textarea',
        '#title' => $i > 0 ? t('Body: version ') . ($i + 1) : t('Body'),
        '#description' => t('The content you would like your supporters to share. <strong>The {LINK} placeholder must be placed somewhere within the text</strong> and will be replaced by the Campaign URL.'),
        '#default_value' => isset($input[$body_id]) ? $input[$body_id] : $variant['email_body'],
        '#maxlength' => 750,
        '#attributes' => array('class' => array('sp-email-text')),
        '#states' => $visible,
      ),
    );
    return $ret;
  }

}
