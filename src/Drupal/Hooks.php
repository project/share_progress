<?php

namespace Drupal\share_progress\Drupal;

/**
 * Drupal hooks.
 */
class Hooks {

  /**
   * Implements hook_menu().
   */
  static public function menu() {
    return array(
      'admin/config/services/share_progress' => array(
        'title' => 'Share Progress',
        'description' => 'Configure Share Progress',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('share_progress_admin'),
        'access arguments' => array('administer share progress'),
        'file' => 'share_progress.admin.inc',
        'type' => MENU_NORMAL_ITEM,
      ),
      'node/%node/share_progress' => array(
        'access callback' => 'node_access',
        'access arguments' => array('update', 1),
        'page callback' => 'drupal_get_form',
        'page arguments' => array('share_progress_edit_form', 1),
        'title' => t('Social'),
        'weight' => 5,
        'type' => MENU_LOCAL_TASK,
      ),
    );
  }

  /**
   * Implements hook_permission().
   */
  static public function permission() {
    return array(
      'administer share progress' => array(
        'title' => t('Administer Share Progress'),
        'description' => t('Administer the Share Progress module.'),
      ),
    );
  }

  /**
   * Implements hook_theme().
   */
  static public function theme() {
    return array(
      'share_progress_analytics_network' => array(
        'render element' => 'table',
        'file' => 'share_progress.theme.inc',
      ),
      'share_progress_analytics_tool' => array(
        'render element' => 'table',
        'file' => 'share_progress.theme.inc',
      ),
      'share_progress_image_preview' => array(
        'render element' => 'element',
        'file' => 'share_progress.theme.inc',
      ),
    );
  }

  /**
   * Implements hook_preprocess_page().
   */
  static public function preprocessPage(&$variables) {
    $current_path = current_path();
    $js_paths = variable_get('share_progress_paths', '');
    $js_paths = preg_split('/\r\n|[\r\n]/', $js_paths);
    $js_snippet = explode('"', variable_get('share_progress_snippet', ''));
    if (isset($js_snippet[1])) {
      foreach ($js_paths as $js_path) {
        if ($js_path == $current_path
          // Check for star at end of js_path.
          || (substr($js_path, -1) == '*'
          // Check for js_path without * in current path.
          && substr($current_path, 0, strlen(substr($js_path, 0, -1))) === substr($js_path, 0, -1))) {
          drupal_add_js($js_snippet[1], 'external');
          drupal_add_css(drupal_get_path('module', 'share_progress') . '/css/share_progress.css');
        }
      }
    }
    else {
      watchdog('Share Progress',
        'Unable to retrieve share progress path from snippet.',
        array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Implements hook_webform_analysis_alter().
   */
  static public function webformAnalysisAlter(&$analysis) {
    $analytics = new Analytics($analysis['#node']->nid);
    $analysis['data'][] = $analytics->tool();
  }

}
