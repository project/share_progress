<?php

namespace Drupal\share_progress\Drupal;

/**
 * Token handling.
 */
class Tokens {
  private $nid;
  private $tokens;

  /**
   * Implements hook_token_info().
   */
  static public function info() {
    $sp_tokens = array();
    $tokens = array('twitter', 'facebook', 'email');
    foreach ($tokens as $token) {
      $sp_tokens['sp_' . $token . '_page_button'] = [
        'name' => t('!t Share Button', ['!t' => ucfirst($token)]),
        'description' => t(
          'Used to add a !t share button to a web page.',
          ['!t' => $token]
        ),
      ];
      $sp_tokens['sp_' . $token . '_email_button'] = [
        'name' => t('!t Email Share Button', ['!t' => ucfirst($token)]),
        'description' => t(
          'Used to add a !t share button to an email.',
          ['!t' => $token]
        ),
      ];
    }

    // Return associative array of tokens & token types.
    return array('tokens' => array('node' => $sp_tokens));
  }

  /**
   * Constructor.
   */
  public function __construct($type, $tokens, array $data = array()) {
    $this->getNid($type, $data);
    $this->tokens = $tokens;
  }

  /**
   * Helper to get the nid.
   */
  private function getNid($type, $data) {
    $nid = '';
    if ($type == 'node') {
      if (isset($data['node'])) {
        $nid = $data['node']->nid;
      }
      elseif (isset($data['entity']) && isset($data['entity_type']) && $data['entity_type'] == 'node') {
        $nid = $data['entity']->nid;
      }
    }
    $this->nid = $nid;
  }

  /**
   * Generate replacement for email and page tokens.
   */
  private function generateMarkup($network, $title, $background) {
    $sp_record = Node::instance()->load($this->nid, $network);
    $replace = $sp_record->buttonMarkup($network, $title, $background);
    return $replace;
  }

  /**
   * Implements hook_tokens().
   */
  public function tokens() {
    $replacements = array();
    // Do nothing if we don't have a nid.
    if (!$this->nid) {
      return $replacements;
    }
    foreach ($this->tokens as $name => $original) {
      switch ($name) {
        case 'sp_facebook_email_button':
        case 'sp_facebook_page_button':
          $replacements[$original] = $this->generateMarkup('facebook', t('Post to Facebook'), '#39579A');
          break;

        case 'sp_twitter_email_button':
        case 'sp_twitter_page_button':
          $replacements[$original] = $this->generateMarkup('twitter', t('Share on Twitter'), '#02ACED');
          break;

        case 'sp_email_email_button':
        case 'sp_email_page_button':
          $replacements[$original] = $this->generateMarkup('email', t('Share by email'), '#14AC2B');
          break;

      }
    }
    return $replacements;
  }

}
