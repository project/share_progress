<?php

namespace Drupal\share_progress\Drupal;

use \Drupal\share_progress\ShareProgress\ShareProgress;

/**
 * Analytics functions for share progress API.
 */
class Analytics {
  private $nid;

  /**
   * Constructor.
   */
  public function __construct($nid) {
    $this->nid = $nid;
  }

  /**
   * Helper to get network titles.
   */
  static public function networkTitle($network) {
    $titles = array(
      'email' => 'E-Mail',
      'facebook' => 'Facebook',
      'twitter' => 'Twitter',
      'dark_social' => 'Click to copy',
    );
    return isset($titles[$network]) ? $titles[$network] : ucfirst($network);
  }

  /**
   * Sum analytics from different networks.
   */
  private function sum($tables) {
    $sums = array(
      'total_shares' => 0,
      'total_viral_visitors' => 0,
      'total_viral_actions' => 0,
      'total_visitors' => 0,
    );
    foreach ($tables as $table) {
      foreach ($table as $child => $data) {
        if (is_array($data) && isset($data['totals'])) {
          foreach (array_keys($sums) as $total) {
            $sums[$total] += $data['totals'][$total];
          }
        }
        elseif ($child == 'twitter' || $child == 'facebook' || $child == 'email') {
          foreach (array_keys($sums) as $total) {
            $sums[$total] += $data[$total];
          }
        }
      }
    }
    return $sums;
  }

  /**
   * Process analytics for a network.
   */
  public function network($network) {
    $node = Node::instance()->load($this->nid, $network);
    if ($node->analyze()) {
      return ShareProgress::instantiate()->analyzeTotals($node->analyze());
    }
    return array();
  }

  /**
   * Process analytics for a node.
   */
  public function tool() {
    $networks = array();
    foreach (array('twitter', 'facebook', 'email') as $network) {
      if ($stats = $this->network($network)) {
        $networks[$network] = $stats;
      }
    }
    $tables = array();
    if (!empty($networks)) {
      $tables = array($this->nid => $networks);
      $tables[$this->nid]['totals'] = $this->sum($tables);
      $tables['#theme'] = 'share_progress_analytics_tool';
    }
    return $tables;
  }

}
