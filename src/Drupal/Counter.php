<?php

namespace Drupal\share_progress\Drupal;

use \Drupal\share_progress\traits\Singleton;
use \Drupal\share_progress\traits\Environment;

/**
 * Counter class.
 */
class Counter {

  use Singleton;
  use Environment;

  /**
   * Add character counter library.
   */
  public function add(&$form) {
    $character_counter = $this->librariesGetPath('word-and-character-counter');
    // Add Word and Character Counter library and associated js if it exists.
    if ($character_counter) {
      $form['#attached']['js'][] = array(
        'data' => $character_counter . '/word-and-character-counter.js',
        'type' => 'file',
      );
      // Add share progress js.
      $path = $this->drupalGetPath('module', 'share_progress');
      $form['#attached']['js'][] = array(
        'data' => $path . '/js/share_progress.js',
        'type' => 'file',
      );
    }
  }

}
