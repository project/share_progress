<?php


namespace Drupal\share_progress\Drupal;

use \Drupal\share_progress\ShareProgress\ShareProgress;
use \Drupal\share_progress\traits\Environment;
use \Drupal\share_progress\traits\Singleton;

/**
 * Class for local share_progress data.
 */
class Node {

  use Environment;
  use Singleton;

  public $nid;
  public $network;
  private $data;

  /**
   * Load node data.
   */
  public function load($nid, $network) {
    $this->nid = $nid;
    $this->network = $network;
    try {
      $this->data = db_select('share_progress', 's')
        ->fields('s')
        ->condition('nid', $this->nid)
        ->condition('type', $this->network)
        ->execute()
        ->fetchAssoc();
    }
    catch (Exception $e) {
      watchdog('share_progress',
        'Failed to load list of share progress records for node %n: $e',
        array('%n' => $this->nid, '%e' => $e->getMessage()), WATCHDOG_ERROR);
      throw $e;
    }
    return $this;
  }

  /**
   * Return markup.
   */
  public function buttonMarkup($network, $title, $background) {
    $markup = '';
    if ($this->get('enabled')) {
      $spid = $this->get('spid');
      $id = variable_get('share_progress_id', '');
      // @codingStandardsIgnoreStart
      $markup .= '<a target="_top" type="' . substr($network, 0, 1) . '" href="https://shpg.org/' . $id . '/' . $spid . '/' . $network . '" style="display:inline-block;width:213px;height:30px;color:#FFFFFF;text-decoration:none;text-align:center;padding:15px 0px 0px;background:' . $background . ';font:normal 17px/17px Arial, Helvetica, sans-serif;box-sizing:content-box;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;">' . t($title) . ' &#9658;</a>';
      // @codingStandardsIgnoreEnd
    }
    return $markup;
  }

  /**
   * Return analysis.
   */
  public function analyze() {
    $data = $this->data();
    return isset($data['spid']) ? $data['spid'] : FALSE;
  }

  /**
   * Return full result.
   */
  public function data() {
    return $this->data;
  }

  /**
   * Return single value.
   */
  public function get($index, $default = FALSE) {
    $data = $this->data();

    if (isset($data[$index])) {
      return $data[$index];
    }
    else {
      return $default;
    }
  }

  /**
   * Main edit form.
   */
  public function editForm($form, &$form_state, $node) {
    $help = [
      '<p>Use the following tokens to add share progress buttons generated here to page content or emails: <ul>
      <li>[node:sp_twitter_page_button]</li>
      <li>[node:sp_facebook_page_button]</li>
      <li>[node:sp_email_page_button]</li>
      <li>[node:sp_twitter_email_button]</li>
      <li>[node:sp_facebook_email_button]</li>
      <li>[node:sp_email_email_button]</li></ul></p>',
      '<div style="padding: 5px;">Click save to update your social sharing settings for twitter, facebook and email.</div>',
    ];
    $nid = $node->nid;

    $form = [
      'introduction' => ['#markup' => $help[0]],
      'twitter' => $this->networkForm($form, $form_state, $nid, 'twitter'),
      'facebook' => $this->networkForm($form, $form_state, $nid, 'facebook'),
      'email' => $this->networkForm($form, $form_state, $nid, 'email'),
      'submit_help_text' => ['#markup' => $help[1]],
      'submit' => ['#type' => 'submit', '#value' => t('Save')],
    ];

    return $form;
  }

  /**
   * Network specific form stuff.
   */
  public function networkForm($form, &$form_state, $nid, $network) {
    // Gather data.
    $input = isset($form_state['values']) ? $form_state['values'] : [];
    $variants = new Variants($this->load($nid, $network));

    // Define labels.
    $label_id = $network . '_id';
    $label_enabled = $network . '_enabled';
    $label_url = $network . '_page_url';
    $label_count = $network . '_count';
    $label_variants = $network . '_variants';

    $spid = $this->get('spid');
    $count = $variants->count();

    $set = array(
      '#type' => 'fieldset',
      '#title' => ucfirst($network) . ' Buttons',
      '#description' => '',
    );

    $default_id = isset($input[$label_id]) ? $input[$label_id] : (isset($spid) ? $spid : '');
    $set[$label_id] = array(
      '#type' => 'hidden',
      '#default_value' => $default_id,
    );

    $set[$label_enabled] = array(
      '#type' => 'checkbox',
      '#title' => 'Enable ' . ucfirst($network) . ' Button(s)',
      '#default_value' => $this->get('enabled'),
    );

    $set[$label_url] = array(
      '#type' => 'textfield',
      '#title' => 'Campaign URL',
      '#description' => t('Insert a URL to the web page where your New/Mode form will be embedded. Include https:// or http:// at the beginning.'),
      '#default_value' => $this->get('page_url'),
    );

    $set[$label_count] = array(
      '#type' => 'select',
      '#title' => 'A/B Testing',
      '#description' => t('Enable A/B testing to automatically optimize social sharing by testing different versions of content.'),
      '#options' => array(
        '1' => t('disabled'),
        '2' => t('2 versions tested'),
        '3' => t('3 versions tested'),
      ),
      '#default_value' => $count,
    );

    $vset = $variants->form($input);
    $set[$label_variants] = $vset;
    return $set;
  }

  /**
   * Network validation.
   */
  public function networkValidate($network, $form, $form_state) {
    $values = $form_state['values'];
    $url = $network . '_page_url';
    $enabled = $values[$network . '_enabled'];
    if ($enabled) {
      if (empty($values[$url])) {
        form_set_error($url,
          t('@b button(s) enabled, but no campaign URL has been set.',
            ['@b' => ucfirst($network)]));
      }
      elseif (!valid_url($values[$url], TRUE)) {
        form_set_error($url, t('Invalid campaign URL'));
      }
      $method = $network . 'Validate';
      $this->{$method}($form, $form_state, $values);
    }
  }

  /**
   * Twitter specific validation.
   */
  public function twitterValidate($form, $form_state, $values) {
    for ($i = 0; $i < $values['twitter_count']; $i++) {
      $share = 'twitter_share_text_' . $i;
      if (empty($values[$share])) {
        form_set_error($share, t('Twitter button(s) enabled and no Twitter share text has been set.'));
      }
      elseif (strpos($values[$share], '{LINK}') === FALSE) {
        form_set_error($share, t('Missing {LINK} token: Twitter share text needs a {LINK} token to indicate where the share link should be added.'));
      }
    }
  }

  /**
   * Facebook specific validation.
   */
  public function facebookValidate($form, $form_state, $values) {
    for ($i = 0; $i < $values['facebook_count']; $i++) {
      $title = 'facebook_title_' . $i;
      if (empty($values[$title])) {
        form_set_error($title, t('Facebook button(s) enabled and no Facebook title has been set.'));
      }
      $desc = 'facebook_description_' . $i;
      if (empty($values[$desc])) {
        form_set_error($desc, t('Facebook button(s) enabled and no Facebook description has been set.'));
      }
    }
  }

  /**
   * Email specific validation.
   */
  public function emailValidate($form, $form_state, $values) {
    for ($i = 0; $i < $values['email_count']; $i++) {
      $subject = 'email_subject_' . $i;
      if (empty($values[$subject])) {
        form_set_error($subject, t('Email button(s) enabled and no email subject has been set.'));
      }
      $body = 'email_body_' . $i;
      if (empty($values[$body])) {
        form_set_error($body, t('Email button(s) enabled and no Email body text has been set.'));
      }
      elseif (strpos($values[$body], '{LINK}') === FALSE) {
        form_set_error($body, t('Missing {LINK} token: Email body needs a {LINK} token to indicate where the share link should be added.'));
      }
    }
  }

  /**
   * Validation handler for form.
   */
  public function validate($form, &$form_state) {
    $this->networkValidate('twitter', $form, $form_state);
    $this->networkValidate('facebook', $form, $form_state);
    $this->networkValidate('email', $form, $form_state);
  }

  /**
   * Submit handler.
   */
  public function submit($form, $form_state) {
    $this->nid = $form_state['build_info']['args'][0]->nid;

    // Make FB thumbnail files non-temporary.
    $fb_count = $form_state['values']['facebook_count'];
    for ($i = 0; $i < $fb_count; $i++) {
      // Ensure that we actually have a file to save.
      if (!empty($form_state['values']['facebook_thumbnail_' . $i])) {
        // Load the file via file.fid.
        $file = file_load($form_state['values']['facebook_thumbnail_' . $i]);
        // Change status to permanent.
        $file->status = FILE_STATUS_PERMANENT;
        // Save.
        file_save($file);
        // Record that the module is using the file.
        file_usage_add($file, 'share_progress', 'node', $this->nid);
      }
    }
    // Update via api if enabled.
    foreach (['twitter', 'facebook', 'email'] as $network) {
      $this->network = $network;
      $this->save($form, $form_state);
    }
  }

  /**
   * Save network settings.
   */
  public function save($form, &$form_state) {

    // Prepare data for push and save.
    $network = $this->network;
    $nid = $this->nid;
    $this->load($nid, $network);
    $original = new Variants($this);

    $values = isset($form_state['values']) ? $form_state['values'] : [];
    $count = isset($values[$network . '_count']) ? $values[$network . '_count'] : 0;
    $enabled = isset($values[$network . '_enabled']) ? $values[$network . '_enabled'] : FALSE;
    $id = isset($values[$network . '_id']) ? $values[$network . '_id'] : '';

    // Gather data even if nothing enabled so that it can be saved as "draft".
    $variants = [];
    for ($i = 0; $i < 3; $i++) {
      // If we go past what was requested.
      if ($i + 1 > $count) {
        // And existing variants exist.
        if ($original->count() && $i + 1 <= $original->count()) {
          // Then mark them as destroyed.
          $variants[$i]['id'] = $original->variants[$i]['id'];
          $variants[$i]['_destroy'] = TRUE;
        }
      }
      else {
        // Only set variant id if we're not doing a draft.
        if (!strstr($id, 'draft') && $values[$network . '_variant_id_' . $i]) {
          $variants[$i]['id'] = $values[$network . '_variant_id_' . $i];
        }
        switch ($network) {
          case 'twitter':
            $message = $values['twitter_share_text_' . $i];
            $variants[$i]['twitter_message'] = $message;
            break;

          case 'email':
            $variants[$i]['email_subject'] = $values['email_subject_' . $i];
            $variants[$i]['email_body'] = $values['email_body_' . $i];
            break;

          case 'facebook':
            $facebook_thumbnail = '';
            if (!empty($values['facebook_thumbnail_' . $i])) {
              $file = file_load($values['facebook_thumbnail_' . $i]);
              $uri = $file->uri;
              $facebook_thumbnail = file_create_url($uri);
            }
            $variants[$i]['facebook_title'] = $values['facebook_title_' . $i];
            $desc = $values['facebook_description_' . $i];
            $variants[$i]['facebook_description'] = $desc;
            $variants[$i]['facebook_thumbnail'] = $facebook_thumbnail;
            break;

        }
      }
    }

    $data = [];
    $data['id'] = $id;
    $data['variants'][$network] = $variants;
    $data['page_url'] = isset($values[$network . '_page_url']) ? $values[$network . '_page_url'] : '';
    $data['auto_fill'] = FALSE;
    switch ($network) {
      case 'twitter':
        $data['button_template'] = 'sp_tw_large';
        break;

      case 'email':
        $data['button_template'] = 'sp_em_large';
        break;

      case 'facebook':
        $data['button_template'] = 'sp_fb_large';
        break;

    }

    if ($enabled) {
      // Remove id if it's a draft.
      if (strstr($data['id'], 'draft')) {
        unset($data['id']);
      }

      // Try to save/update via SP API.
      $response = ShareProgress::instantiate()->update($data);

      if (!$response) {
        // Attempt a hack to force the issue.
        // Sleeps are rate limit protection.
        sleep(2);
        $data['page_url'] = 'https://google.com';
        $response = ShareProgress::instantiate()->update($data);
        $data['page_url'] = isset($values[$network . '_page_url']) ? $values[$network . '_page_url'] : '';
        if ($response) {
          $data['id'] = $response['id'];
          sleep(2);
          $response = ShareProgress::instantiate()->update($data);
        }
      }

      // If successful, upsert DB with new data.
      if ($response) {
        $this->merge($response, $values);
      }
      else {
        $this->saveDraft($data, $values);
        watchdog(
          'Share Progress',
          'Failed to update share progress records using API on node %n.',
          ['%n' => $nid],
          WATCHDOG_ERROR
        );
        drupal_set_message(
          t('An error occurred when attempting to save your %n data.', ['%n' => $network]),
          'error'
        );
      }
    }
    else {
      $this->saveDraft($data, $values);
    }
  }

  /**
   * Add file IDs back in to variants before saving.
   */
  private function fixFileIds($variants, $values) {
    if ($this->network == 'facebook') {
      foreach ($variants['facebook'] as $key => $variant) {
        $thumb = 'facebook_thumbnail_' . $key;
        if (isset($values[$thumb])) {
          $variants['facebook'][$key]['facebook_thumbnail'] = $values[$thumb];
        }
      }
    }
    return $variants;
  }

  /**
   * Merge a live record into the db.
   */
  private function merge($response, $values) {
    $variants = $this->fixFileIds($response['variants'], $values);
    try {
      $query = db_merge('share_progress')
        ->key(array(
          'nid' => $this->nid,
          'type' => $this->network,
        ))
        ->fields(array(
          'spid' => $response['id'],
          'type' => $this->network,
          'nid' => $this->nid,
          'updated' => REQUEST_TIME,
          'page_url' => $response['page_url'],
          'share_button_html' => $response['share_button_html'],
          'variants' => serialize($variants),
          'advanced_options' => serialize($response['advanced_options']),
          'enabled' => 1,
        ));
      $query->execute();
      drupal_set_message(
        t('Your %n share settings have been updated.',
        ['%n' => $this->network]),
        'status'
      );
    }
    catch (Exception $e) {
      watchdog(
        'Share Progress',
        'Failed to write share progress record to DB for node %n: %e',
        ['%n' => $this->nid, '%e' => $e->getMessage()],
        WATCHDOG_ERROR
      );
      drupal_set_message(
        t('An error occurred when attempting to save your %n data. Please contact a system administrator to address this issue.',
        ['%n' => $this->network]),
        'error'
      );
    }
    return TRUE;
  }

  /**
   * Save data as a draft.
   */
  private function saveDraft($data, $values) {
    $data['variants'] = $this->fixFileIds($data['variants'], $values);
    // Delete button if this is not a draft already.
    if ($data['id'] && !strstr($data['id'], 'draft')) {
      ShareProgress::instantiate()->delete($data['id']);
    }
    // Don't save deleted variants.
    foreach ($data['variants'] as $network => $variants) {
      foreach ($variants as $key => $variant) {
        if (isset($variant['_destroy'])) {
          unset ($data['variants'][$network][$key]);
        }
      }
    }
    // Store to db.
    try {
      // Delete real buttons and replace with draft buttons.
      db_delete('share_progress')
        ->condition('nid', $this->nid)
        ->condition('type', $this->network)
        ->execute();
      if ($data['page_url'] || !empty($data['variants'])) {
        db_insert('share_progress')
          ->fields(['spid', 'type', 'nid', 'updated', 'page_url', 'variants'])
          ->values([
            'spid' => $this->network . '_draft_' . $this->nid,
            'type' => $this->network,
            'nid' => $this->nid,
            'updated' => REQUEST_TIME,
            'page_url' => $data['page_url'],
            'variants' => serialize($data['variants']),
          ])
          ->execute();
        drupal_set_message(
          t('Your %n share settings have been saved as disabled.',
          ['%n' => $this->network]), 'status'
        );
      }
    }
    catch (Exception $e) {
      watchdog(
        'Share Progress',
        'Failed to save draft share progress record to DB for node %n: %e',
        ['%n' => $this->nid, '%e' => $e->getMessage()],
        WATCHDOG_ERROR
      );
      drupal_set_message(
        t('An error occurred when attempting to save your %n data. Please contact a system administrator to address this issue.',
        ['%n' => $this->network]),
        'error'
      );
    }
  }

}
