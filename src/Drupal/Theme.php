<?php

namespace Drupal\share_progress\Drupal;

/**
 * Theme functions for Analytics functions for share progress API.
 */
class Theme {

  /**
   * Instantiator.
   */
  static public function instantiate() {
    static $ob;
    if (!$ob) {
      $ob = new self();
    }
    return $ob;
  }

  /**
   * Render table.
   */
  private function render($table) {
    $headers = array(
      'Type',
      'Shares',
      'Viral visitors',
      'Viral actions',
      'Total Visitors',
    );
    foreach ($table as $key => $data) {
      if (is_array($data)) {
        $title = Analytics::networkTitle($key);
        $row = array(array('data' => $title, 'header' => TRUE));
        if ($key != 'totals') {
          // API returns totals in a weird order.
          $total_visitors = array_shift($data);
          $data['total_visitors'] = $total_visitors;
        }
        $row = array_merge($row, array_values($data));
        $rows[] = $row;
      }
    }
    $ret['analytics']['table'] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    );
    return $ret;
  }

  /**
   * Network theme (not currently used)
   */
  public function analyticsNetwork($variables) {
    $renderable = $this->render($variables['table']);
    return drupal_render($renderable);
  }

  /**
   * Tool theme.
   */
  public function analyticsTool($variables) {
    $ret['analytics'] = array(
      '#type' => 'fieldset',
      '#title' => 'Share Progress Analytics',
      '#attributes' => array(
        'class' => array('collapsible'),
      ),
    );
    foreach ($variables['table'] as $data) {
      if (is_array($data)) {
        $renderable = $this->render($data);
        $ret['analytics']['table'] = $renderable;
      }
    }
    return drupal_render($ret);
  }

  /**
   * Image preview for uploaded images.
   */
  static public function imagePreview($variables) {
    // Isolate element.
    $element = $variables['element'];
    // If we have an image then add an image preview.
    if (isset($element['#file']->uri)) {
      $output = '<div id="edit-logo-ajax-wrapper"><div class="form-item form-type-managed-file form-item-logo"><span class="file">';
      $output .= '<img height="50px" src="' . image_style_url('large', $element['#file']->uri) . '" />';
      $output .= '</span></div>';
      // Add standard remove button and link.
      $output .= drupal_render_children($element);
      return $output;
    }
  }

}
