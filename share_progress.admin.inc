<?php

/**
 * @file
 * Admin functions for share progress API.
 */

/**
 * Admin form for setting API key.
 */
function share_progress_admin() {
  $form = array();

  $form['share_progress_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Share Progress Key'),
    '#default_value' => variable_get('share_progress_key', ''),
    '#description' => t("Your Share Progress API key."),
    '#required' => TRUE,
  );

  $form['share_progress_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Share Organization ID'),
    '#default_value' => variable_get('share_progress_id', ''),
    '#description' => t("Your Share Organization ID (this should be visible in your Share Progress snippet)."),
    '#required' => TRUE,
  );

  $form['share_progress_snippet'] = array(
    '#type' => 'textfield',
    '#title' => t('Share Progress Snippet'),
    '#default_value' => variable_get('share_progress_snippet', ''),
    '#description' => t("Your Share Progress snippet."),
    '#required' => TRUE,
  );

  $form['share_progress_js_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Share Progress JavaScript URL'),
    '#default_value' => variable_get('share_progress_js_url', ''),
    '#description' => t("Your Share Progress JavaScript URL (looks something like //c.shpg.org/000/sp.js)."),
  );

  $form['share_progress_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Share Progress Paths'),
    '#default_value' => variable_get('share_progress_paths', 'node/*'),
    '#description' => t("Please add any paths, one per line, that share progress should be added to. Format should be node/125 without a trailing slash. * may be used as a wildcard at the end of paths."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
