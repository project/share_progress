(function (window, Drupal, $) {

  Drupal.behaviors.shareProgressCounter = {
    attach: function (context, settings) {

      // For each twitter text area.
      $(".sp-twitter-text").each(function (index) {
        // Calculate limit.
        var twitter_limit =  258;
        // Invoke counter.
        $(this).once().counter({
          type: 'char',
          goal: twitter_limit,
          msg: Drupal.t('characters remaining'),
          append: false
        });
      });

      // For each facebook text area.
      $(".sp-facebook-text").each(function (index) {
        // Calculate limit.
        var facebook_limit =  260;
        // Invoke counter.
        $(this).once().counter({
          type: 'char',
          goal: facebook_limit,
          msg: Drupal.t('characters remaining'),
          append: false
        });
      });
    }
  };
}(window, Drupal, jQuery));
