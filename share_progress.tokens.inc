<?php

/**
 * @file
 * Tokens for Integration with share progress API.
 */

use \Drupal\share_progress\Drupal\Tokens;

/**
 * Implements hook_token_info().
 */
function share_progress_token_info() {
  Tokens::info();
}

/**
 * Implements hook_tokens().
 */
function share_progress_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $ob = new Tokens($type, $tokens, $data);
  return $ob->tokens();
}
